<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
        <?php
        session_start();
        if (isset($_SESSION['username'])) {
            echo "
            
            <div class='container'>
                <form action='quary.php' method='POST' class='form-group'>
                    <h3 class='mt3'>Input Database</h3>
                    <label for=''>Name</label>
                    <input type='text' class='form-control' name='name'>

                    <label for=''>Gender</label>
                    <select name='gender' class='form-control'>
                        <option value='M'>Male</option>
                        <option value='F'>Female</option>
                    </select>

                    <label for='' >Address</label>
                    <textarea name='address' class='form-control'></textarea>

                    <button type='submit' class='btn btn-primary mt-3'>Submit</button>
                    <button type='submit' class='btn btn-danger mt-3'><a href='logOut.php' style='text-decoration: none;color: white;'>Log Out</a></button>

                </form>
            </div>
            
            ";
        } else {
            header("location:loginForm.html");
        }
        ?>
</body>
</html>
