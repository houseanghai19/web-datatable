<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Responsive Sidebar Dashboard Template</title>
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" charset="utf-8"></script>

  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="js/bootstrap.min.js"></script>

  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

  <script>
    function confirmMessage() {
      return confirm("Are you Sure want to delete?");
    }
  </script>
</head>

<body>

  <input type="checkbox" id="check">
  <!--header area start-->
  <header>
    <label for="check">
      <i class="fas fa-bars" id="sidebar_btn"></i>
    </label>
    <div class="left_area">
      <h3>SCHOOL <span>MANAGEMENT</span></h3>
    </div>
    <div class="right_area">
      <button type='submit' class='logout_btn'>
        <a href='logOut.php' style='text-decoration: none;color: white;'>Log Out</a>
      </button>

    </div>
  </header>
  <!--header area end-->
  <!--mobile navigation bar start-->
  <div class="mobile_nav">
    <div class="nav_bar">
      <img src="1.png" class="mobile_profile_image" alt="">
      <i class="fa fa-bars nav_btn"></i>
    </div>
    <div class="mobile_nav_items">
      <a href="#"><i class="fas fa-desktop"></i><span>Dashboard</span></a>
      <a href="#"><i class="fas fa-cogs"></i><span>Components</span></a>
      <a href="#"><i class="fas fa-table"></i><span>Tables</span></a>
      <a href="#"><i class="fas fa-th"></i><span>Forms</span></a>
      <a href="#"><i class="fas fa-info-circle"></i><span>About</span></a>
      <a href="#"><i class="fas fa-sliders-h"></i><span>Settings</span></a>
    </div>
  </div>
  <!--mobile navigation bar end-->
  <!--sidebar start-->
  <div class="sidebar">
    <div class="profile_info">
      <img src="1.jpg" class="profile_image" alt="">
      <h4>Administrator</h4>
    </div>
    <a href="#"><i class="fas fa-desktop"></i><span>Dashboard</span></a>
    <a href="#"><i class="fas fa-cogs"></i><span>Components</span></a>
    <a href="#"><i class="fas fa-table"></i><span>Tables</span></a>
    <a href="#"><i class="fas fa-th"></i><span>Forms</span></a>
    <a href="#"><i class="fas fa-info-circle"></i><span>About</span></a>
    <a href="#"><i class="fas fa-sliders-h"></i><span>Settings</span></a>
  </div>
  <!--sidebar end-->

  <div class="content">
    <div class="card">
      <?php
      session_start();
      if (isset($_SESSION['username'])) {
        try {
          $host = '127.0.0.1';
          $username = 'admin';
          $pwd = '123';
          $dbname = 'haidb';

          $conn = new PDO("mysql:host=$host;dbname=$dbname", $username, $pwd);
          $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

          // $mysql="insert into students(name,gender,address) values('$name','$gender','$address')";
          // $conn->exec($mysql);
          // echo 'You inputed filed '.$conn->lastInsertId();

          //Quary
          $mysql = "select * from students";
          $stmt = $conn->query($mysql);



          echo
          "<div class='container'>" .

            "<h3 class='mt-4  '>Student Information</h3>" .

            //<!-- Button trigger modal -->
            '<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                    Add New Student
                </button>' .

            // <!-- Modal -->
            '<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Add New Student</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                        <div class="modal-body">' .

            "<div class='container'>
                                <form action='insert.php' method='POST' class='form-group'>
                                    <h3 class='mt3'>Input Database</h3>

                                    <label for=''></label>
                                    <input type='text' class='form-control' name='id' hidden >

                                    <label for=''>Name</label>
                                    <input type='text' class='form-control' name='name'>
                
                                    <label for=''>Gender</label>
                                    <select name='gender' class='form-control'>
                                        <option value='M'>Male</option>
                                        <option value='F'>Female</option>
                                    </select>
                
                                    <label for='' >Address</label>
                                    <textarea name='address' class='form-control'></textarea>
                
                                    
                                    <div class='modal-footer'>
                                        <button type='submit' class='btn btn-primary'>Add</button>
                                        <button type='button' class='btn btn-secondary' data-bs-dismiss='modal'>Close</button>
                                    </div>
                
                                </form>" .
            '</div>                           
                                
                            </div>
                        </div>
                    </div>
                </div>' .

            //form-quary
            "<table class='table table-border container mt-3'>" .
              '<thead>' .
                '<tr>' .
                  '<td>ID</td>' .
                  '<td>Name</td>' .
                  '<td>Gender</td>' .
                  '<td>Address</td>' .
                  '<td>Action</td>' .
                '</tr>' .
              '</thead>' .
            '<tbody>';
          while ($row = $stmt->fetch()) {
            echo '<tr>' .
                '<td>' . $row['id'] . '</td>' .
                '<td>' . $row['name'] . '</td>' .
                '<td>' . $row['gender'] . '</td>' .
                '<td>' . $row['address'] . '</td>' .
                '<td>' .

              ' <button class="btn btn-primary">
                                    <a style="color:white; text-decoration:none;" href="edit.php?id=' . $row['id'] . '">Edit</a>
                </button> &nbsp' .

              '<form action="delete.php" method="post" onsubmit=' . '"return confirmMessage()"' . '>' .
                '<input name="id" type="hidden" value="' . $row['id'] . '">' .
                '<button class="btn btn-danger" type="submit">Delete</button>' .
              '</form>' .


              '</td>' .
              '</tr>';
          }
          echo '</tbody></table></div>';
          

          $conn = null;
        } catch (PDOException $e) {
          echo $e->getMessage();
        }
      } else {
        header("location:loginForm.html");
      }
      ?>
    </div>

  </div>

  <script type="text/javascript">
    $(document).ready(function() {
      $('.nav_btn').click(function() {
        $('.mobile_nav_items').toggleClass('active');
      });
    });
  </script>

</body>

</html>