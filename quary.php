<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Database</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/bootstrap.min.js"></script>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

    <script>
        function confirmMessage(){
            return confirm("Are you Sure want to delete?");
        }
    </script>



</head>

<body>
    <?php
    session_start();
    if (isset($_SESSION['username'])) {
        try 
        {
            $host = '127.0.0.1';
            $username = 'admin';
            $pwd = '123';
            $dbname = 'haidb';

            $conn = new PDO("mysql:host=$host;dbname=$dbname", $username, $pwd);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            // $mysql="insert into students(name,gender,address) values('$name','$gender','$address')";
            // $conn->exec($mysql);
            // echo 'You inputed filed '.$conn->lastInsertId();

            //pagenation
            //step1
            $page=1;
            if(isset($_GET['page'])){
                $page=$_GET['page'];
            }

            //step2
            $row_per_page=3;
            $sql="select count(*) from students";
            $stmt=$conn->query($sql);
            $recordCount=$stmt->fetchColumn(0);

            //step3
            $totalPage=ceil($recordCount/$row_per_page);

            //step4
            $offset=($page-1)*$row_per_page;

            $sql="select * from students limit".$row_per_page.$offset;





            //Quary
            // $mysql = "select * from students";
             $stmt = $conn->query($sql);

            

            echo
            "<div class='container mt-4'>" .

                "<h3 class='mt-3'>Student Information</h3>" .

                //<!-- Button trigger modal -->
                '<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                    Add New Student
                </button>'.

               // <!-- Modal -->
                '<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Add New Student</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                        <div class="modal-body">' .

                "<div class='container'>
                                <form action='insert.php' method='POST' class='form-group'>
                                    <h3 class='mt3'>Input Database</h3>

                                    <label for=''></label>
                                    <input type='text' class='form-control' name='id' hidden >

                                    <label for=''>Name</label>
                                    <input type='text' class='form-control' name='name'>
                
                                    <label for=''>Gender</label>
                                    <select name='gender' class='form-control'>
                                        <option value='M'>Male</option>
                                        <option value='F'>Female</option>
                                    </select>
                
                                    <label for='' >Address</label>
                                    <textarea name='address' class='form-control'></textarea>
                
                                    
                                    <div class='modal-footer'>
                                        <button type='submit' class='btn btn-primary'>Add</button>
                                        <button type='button' class='btn btn-secondary' data-bs-dismiss='modal'>Close</button>
                                    </div>
                
                                </form>".
                '</div>                           
                                
                            </div>
                        </div>
                    </div>
                </div>' .

                //form-quary
                "<table class='table table-border container mt-3'>" .
                    '<thead>' .
                        '<tr>' .
                            '<td>ID</td>' .
                            '<td>Name</td>' .
                            '<td>Gender</td>' .
                            '<td>Address</td>' .
                            '<td>Action</td>' .
                        '</tr>' .
                    '</thead>' .
                    '<tbody>';
                     while ($row = $stmt->fetch()) {
                    echo '<tr>' .
                            '<td>' . $row['id'] . '</td>' .
                            '<td>' . $row['name'] . '</td>' .
                            '<td>' . $row['gender'] . '</td>' .
                            '<td>' . $row['address'] . '</td>' .
                            '<td>'.
                        
                               ' <button class="btn btn-primary">
                                    <a style="color:white; text-decoration:none;" href="edit.php?id='.$row['id'].'">Edit</a>
                                </button>

                                &nbsp'.

                                '<form action="delete.php" method="post" onsubmit='.'"return confirmMessage()"'.'>'.
                                    '<input name="id" type="hidden" value="'.$row['id'].'">'.
                                    '<button class="btn btn-danger" type="submit">Delete</button>'.
                                '</form>'.


                            '</td>'.
                        '</tr>';
        }
            echo '</tbody></table>';
            echo "<button type='submit' class='btn btn-danger mt-3'>
                        <a href='logOut.php' style='text-decoration: none;color: white;'>Log Out</a>
                    </button>";
            echo "<nav aria-label='Page navigation example'>
                        <ul class='pagination justify-content-center'>";
                if($page>1)
                {
                    $prev=$page-1;
                    echo "<li class='page-item'><a href='quary.php?page=$prev' class='page-link'>Prev</a></li>'";
                }
                
                for($i=1;$i<=$totalPage;$i++)
                {
                    if($i==$page)
                    {
                        echo "<li class='active page-item'><a class='page-link' href='quary.php?page=$i'>".$i."</a></li>"; 

                    }
                    else
                    {
                        echo "<li class='page-item'><a class='page-link' href='quary.php?page=$i'>".$i."</a></li>"; 
                    }
                 
                 
                }
                if($page<$totalPage)
                {
                    $next=$page+1;
                    echo "<li class='page-item'><a href='quary.php?page=$next' class='page-link'>next</a></li>'";
                }
                echo "</ul></nav>";
        
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        $conn = null;
        
    } else 
    {
        header("location:loginForm.html");
    }
    
    ?>
    
</body>

</html>