<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>

<body>
    <?php

     session_start();
     if (isset($_SESSION['username'])) {
        include('db.php');
        $id = $_GET['id'];

        $conn = db();
        $mysql = "select * from students where id=$id";

        $stmt = $conn->query($mysql);

        
        $name = '';
        $gender = '';
        $address = '';
        while ($row = $stmt->fetch()) {
           
            $name = $row['name'];
            $gender = $row['gender'];
            $address = $row['address'];
        }
        $conn = null;
     } else {
         header("location:loginForm.html");
     }

    ?>
    
    
    <div class='container'>
        <form action='update.php' method='POST' class='form-group'>
            <h3 class='mt-3'>Edit Database</h3>
            <label for=''></label>
            <input type='text' class='form-control' name='student_id' value="<?php echo $id; ?>" hidden >

            <label for=''>Name</label>
            <input type='text' class='form-control' name='name' value="<?php echo $name; ?>">

            <label for=''>Gender</label>
            <select name='gender' class='form-control' value="<?php echo $gender; ?>">
                <option value='M'>Male</option>
                <option value='F'>Female</option>
            </select>

            <label for=''>Address</label>
            <textarea name='address' class='form-control'><?php echo $address; ?></textarea>

            <button type='submit' class='btn btn-primary mt-3'>Save Change</button>
        </form>
    </div>
</body>

</html>